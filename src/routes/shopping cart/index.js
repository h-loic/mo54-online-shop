import { useHomeData } from '../../context/context';
import style from './style.css';
import CartItem from '../../components/cartItem';
import { useEffect, useState } from 'preact/hooks';
import { Link } from 'preact-router/match';

const ShoppingCart = () => {

	const {productsCart,setProductsCart,setPrice} = useHomeData();
	const [totalPrice,setTotalPrice] = useState(0);

	useEffect(() => {
		calculatePrice();
	  }, [productsCart]);

	const handleChangeQuantity = (value,id) => {
		const products = productsCart.map(obj => {
			if (obj._id == id) {
				return {...obj, quantity: value};
			}
			return obj;
			});
		setProductsCart(products);
    }

	const calculatePrice = () => {
		let price = 0
		productsCart.map(product =>{
			price = price + product.price * product.quantity;
		}) 	
		setPrice(price);
		setTotalPrice(price);
	}

	return (
		<div>
			<div class={style.container} style="margin-top : 50px; padding : 20px">
			<h1 class={style.title}>Mon panier</h1>
				{productsCart.length == 0 ?
					<div>Votre panier est vide</div>
					:
					productsCart.map(product =>{
						return <CartItem product={product} HandleChangeQuantity={handleChangeQuantity}/>
					}) 		
				}
				{productsCart.length == 0 ?
					<div/>
					:
					<>
						<h2 class={style.total_price}>Total : {totalPrice}$</h2>
						<div class={style.btn_container}>
							<Link class={style.command_btn} href="/payment">Passer la commande</Link>
						</div>
					</>
				}
			</div>
		</div>
	);
}

export default ShoppingCart;

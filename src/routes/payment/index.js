import { useHomeData } from '../../context/context';
import style from './style.css';
import { useState } from 'preact/hooks';

const Payment = () => {

    const {price} = useHomeData();
    const [name,setName] = useState("");
    const [firstname,setFirstName] = useState("");
    const [adress,setAdress] = useState("");
    const [postalCode,setPostalCode] = useState("");
    const [city,setCity] = useState("");
    const [cardName,setCardName] = useState("");
    const [cardNumber,setCardNumber] = useState("");
    const [CVC,setCVC] = useState("");
    const [expiration,setExpiration] = useState(null);

	return (
		<div>
			<div class={style.container} style="margin-top : 50px; padding : 20px">
                <div class={style.recap}>
                    <span>prix : {price}$</span> 
                </div>
			    <h1 class={style.title}>Adresse de livraison</h1>
                    <label>Nom</label>
                    <input class={style.input} type="text" name="firstname" placeholder="nom..." onChange={e => setName(e.target.value)} />
                    <label>Prénom</label>
                    <input class={style.input} type="text" name="lastname" placeholder="prénom..." onChange={e => setFirstName(e.target.value)}/>
                    <label>Adresse</label>
                    <input class={style.input} type="text" name="adress" placeholder="adresse..." onChange={e => setAdress(e.target.value)}/>
                    <label>Code postal</label>
                    <input class={style.input} type="text" name="postal" placeholder="code postal..." onChange={e => setPostalCode(e.target.value)}/>
                    <label>Ville</label>
                    <input class={style.input} type="text" name="city" placeholder="ville..." onChange={e => setCity(e.target.value)}/>
                <h1 class={style.title}>Paiement</h1>
                    <label>Titulaire de la carte</label>
                    <input class={style.input} type="text" name="nameCard" placeholder="" onChange={e => setCardName(e.target.value)} />
                    <label>Numéro de la carte</label>
                    <input class={style.input} type="text" name="cardNumber" placeholder="" onChange={e => setCardNumber(e.target.value)} />
                    <div class={style.input_container}>
                        <div class={style.small_input_container}>                        
                            <label>CVC</label>
                            <input class={style.small_input} type="text" name="CVC" placeholder="xxx" onChange={e => setCVC(e.target.value)} />
                        </div>
                        <div class={style.medium_input_container}>                        
                            <label>expiration</label>
                            <input class={style.small_input} type="month" name="CVC" placeholder="mm" onChange={e => setExpiration(e.target.value)} />
                        </div>
                    </div>
                <button class={style.payment_btn}>Payer</button>
			</div>
		</div>
	);
}

export default Payment;

import style from './style.css';
import ProductGrid from '../../components/productGrid'
import {useState} from "preact/hooks";
import Search from '../../components/search';
import axios from 'axios';
import { useHomeData } from '../../context/context';

const Home = () => {

	const {categorySelected,productsLoaded,setProductsLoaded} = useHomeData();

	const [categories,setCategories] = useState(
		[{name : "voiture", child : 
			[{name : "freinage", child : 
				[{name : "disque"},{name : "plaquette"}]
			},
			{name : "filtre", child : 
				[{name : "filtre à huilde"},{name : "filtre à air"}]
			}]
	}])

	const [products, setProducts] = useState([]);

	const loadParts = () => {
		let isDataLoaded = false;
		if(productsLoaded.length != 0){
			productsLoaded.map(productLoaded =>{
				if(productLoaded.categorySelected == categorySelected){
					isDataLoaded = true;
					setProducts(productLoaded.partsLoaded);
				}
			})
		}
		if (!isDataLoaded){
			axios.get('http://localhost:5000/api/parts?category='+ 
			categorySelected +
			'&lastId='+ (products.length > 0 ? products[products.length-1]._id : '') )
				.then(res => {
				let parts = res.data;
				setProducts(parts);
				const partsLoaded = [...parts];
				productsLoaded.push({"categorySelected" : categorySelected, partsLoaded});
				setProductsLoaded(productsLoaded);
			})
		}
	}

	const loadMore = () => {
		axios.get('http://localhost:5000/api/parts?category='+ 
		categorySelected +
		'&lastId='+ (products.length > 0 ? products[products.length-1]._id : '') )
			.then(res => {
			let parts = res.data;
			const thisProductsLoaded = productsLoaded.find(productLoaded => {
				return productLoaded.categorySelected === categorySelected;
			  });
			parts.map(part => {
				setProducts(current => [...current, part]);
				thisProductsLoaded.partsLoaded.push(part);
			})
			const newProductsLoaded = productsLoaded.map(product => {
				if (product.categorySelected === categorySelected) {
				  return {...product,thisProductsLoaded};
				}
				return product;
			  });
			  setProductsLoaded(newProductsLoaded);
		})
	}

	return (
		<div>
			<div class={style.home} style="margin-top : 50px; padding : 20px">
				<h1 class={style.title}>Pièces disponibles</h1>
				<div class={style.content}>
					<Search categories={categories} loadFunction={loadParts} categorySelected={categorySelected}/>
					{ products.length == 0 ? 
						<div class={style.no_text_content}><div class={style.title}>Choississez une catégorie de pièce</div></div> :
						<ProductGrid products={products} loadMorefunction={loadMore}/>	
					}
				</div>
			</div>
		</div>
	);
}

export default Home;

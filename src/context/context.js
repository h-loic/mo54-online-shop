import React, { useState, useContext, createContext } from 'react';

const HomeDataContext = createContext();

function HomeDataProvider(props) {
  const [categorySelected, setCategorySelected] = useState("aucune");
  const [productsLoaded,setProductsLoaded] = useState([]);
  const [productsCart,setProductsCart] = useState([])
  const [price,setPrice] = useState(0);

  return <HomeDataContext.Provider value={{categorySelected,setCategorySelected,productsLoaded,setProductsLoaded,productsCart,setProductsCart,price,setPrice}} {...props} />;
}


function useHomeData() {
  const context = useContext(HomeDataContext);
  if (!context) {
    throw new Error('useData must be used within a OrderInfoProvider');
  }
  return context;
}

export { HomeDataProvider, useHomeData };

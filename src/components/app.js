import { h } from 'preact';
import { Router } from 'preact-router';
import Header from './header';

// Code-splitting is automated for `routes` directory
import Home from '../routes/home';
import ShoppingCart from '../routes/shopping cart';
import { HomeDataProvider } from '../context/context';
import Payment from '../routes/payment';

const App = () => (
	<div id="app">
		<Header />
		<HomeDataProvider>
		<Router>
			<Home path="/" />
			<ShoppingCart path="/shoppingCart/" />
			<Payment path="/payment"/>
		</Router>
		</HomeDataProvider>
	</div>
)

export default App;

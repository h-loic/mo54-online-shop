import { useHomeData } from '../../context/context';
import style from './style.css';

const CartItem = (props) => {

    const imgStyle = "background-image: url(assets/img/"+ props.product.img +")";

	return (
		<div>
            <div class={style.blog_card}>
                <div class={style.meta}>
                <div class={style.photo} style={imgStyle}></div>
                </div>
                <div class={style.description}>
                    <h1>{props.product.name}</h1>
                    <h2>{props.product.price}$</h2>
                    <p class={style.read_more}>
                        <div class={style.quantity_text}>quantité :{props.product.quantity}</div>
                        <select id={props.product.id} name={props.product.name} value={props.product.quantity} onChange={(e) => props.HandleChangeQuantity(e.target.value,props.product._id)} class={style.quantity}>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                            <option value={4}>4</option>
                            <option value={5}>5</option>
                            <option value={6}>6</option>
                            <option value={7}>7</option>
                            <option value={8}>8</option>
                            <option value={9}>9</option>
                        </select>
                    </p>
                </div>
            </div>
		</div>
	);
}

export default CartItem;

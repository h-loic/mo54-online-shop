import style from './style.css';
import {useEffect, useState} from "preact/hooks";
import { useHomeData } from '../../context/context';

const ProductCard = (props) => {

    const {productsCart,setProductsCart} = useHomeData();

    const [isAdded,setIsAdded] = useState(false)

    const AddToCart = () => {
        props.product.quantity = 1;
        setProductsCart(previousState => ([...previousState, props.product]))
    }

    const RemoveToCart = () => {
        var array = [...productsCart]; // make a separate copy of the array
        var index = array.indexOf(props.product)
        if (index !== -1) {
          array.splice(index, 1);
          setProductsCart(array);
        }
    }

    useEffect(() => {
        setIsAdded(productsCart.includes(props.product))
    });

    return (
        <div class={style.product_card_container}>
            <div class={style.card}>
                <div class={style.card_image}><img src={"assets/img/" + props.product.img}/></div>
                <div class={style.card_content}>
                    <h2 class={style.card_price}>{props.product.price}$</h2>
                    <p class={style.card_text}>{props.product.name}</p>
                    { isAdded ?
                        <button class={style.card_btn_delete} onClick={() => RemoveToCart()}>Retirer du panier</button>
                        :
                        <button class={style.card_btn} onClick={() => AddToCart()}>Ajouter au panier</button>
                    } 
                </div>
          </div>
        </div>
    );
}

export default ProductCard;
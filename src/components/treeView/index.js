import TreeLevel from "./treeLevel";

const TreeView = (props) => {
	
	return (
		<div>
            <TreeLevel categories={props.categories} index={0} />
		</div>
	);
}

export default TreeView;

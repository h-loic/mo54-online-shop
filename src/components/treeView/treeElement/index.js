import { useState } from 'preact/hooks';
import style from './style.css';
import TreeLevel from '../treeLevel';
import { useHomeData } from '../../../context/context';

const TreeElement = (props) => {

    const {setCategorySelected} = useHomeData();

    const [extended,setExtended] = useState(false);

    const handleClick = (name) => {
        setExtended(!extended);
        setCategorySelected(name);
    }

	return (
        <div>
            <div style={{"margin-left" : 15*props.index + "px"}} onClick={() => setExtended(!extended)} class={style.tree_element}>
                {props.categories == undefined ? 
                    <span></span> : extended ?
                    <span class={style.puce}>&#957;</span> :  
                    <span class={style.puce}>&#62;</span>} 
                    <span>{props.name} </span>
                    <input onClick={() => handleClick(props.name)} name="category" class={style.tree_element_button} type="radio"></input>
            </div>
            {extended ? <TreeLevel categories={props.categories} index={props.index + 1} /> : <div/>}
        </div>
	);
}

export default TreeElement;

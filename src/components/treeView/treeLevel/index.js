import TreeElement from "../treeElement";

const TreeLevel = (props) => {
	
	return (
		<div>
            {props.categories == undefined ? 
            <div></div> 
            :
                <div>
                    {props.categories.map((category) =>{
                        return <TreeElement name={category.name} categories={category.child} index={props.index}/>
                    })}
                </div>
            }
		</div>
	);
}

export default TreeLevel;

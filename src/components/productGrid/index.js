import style from './style.css';
import ProductCard from '../productCard';

const ProductGrid = (props) => {

    const handleLoadMore = () => {
        props.loadMorefunction();
    }

    return (
        <div class={style.product_grid}>
            <div class={style.productGridContainer}>
                {props.products.map((product) =>{
                    return <ProductCard product={product} />
                })}
            </div>
            <button class={style.search_more_btn} onClick={handleLoadMore}>Charger plus de produits</button>
        </div>
    );
}

export default ProductGrid;
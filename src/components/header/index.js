import { Link } from 'preact-router/match';
import style from './style.css';

const Header = () => (

	<header class={style.header}>
		<div class={style.responsive_container}>
			<h1>BestShop</h1>
				<nav>
					<Link activeClassName={style.active} href="/">Accueil</Link>
					<Link activeClassName={style.active} href="/shoppingCart">Mon panier</Link>
				</nav> 
		</div>
	</header>
);

export default Header;

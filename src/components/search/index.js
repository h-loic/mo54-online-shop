import { useState } from 'preact/hooks';
import TreeView from '../treeView';
import style from './style.css';

const Search = (props) => {

    const [carModel,setCarModel] = useState("");
    const [carBrand,setCarBrand] = useState("");

    const handleSearch = () => {
        props.loadFunction()
    }

    return (
        <div class={style.search_container}>
            <div class={style.search_title}>Rechercher votre véhicule</div>
            <select name="car-model" class={style.search_select} onChange={e=> setCarModel(e.target.value)}>
                <option value="">Marques</option>
                <option value="audi">Audi</option>
                <option value="bmw">BMW</option>
                <option value="citroen">Citroen</option>
                <option value="dacia">Dacia</option>
                <option value="fiat">Fiat</option>
                <option value="ford">Ford</option>
            </select>
            { carModel == "" ? 
                <select name="car-model" class={style.search_select} disabled={true}>
                    <option value="">Choississez une marque</option>
                </select>
            : 
                <select name="car-model" class={style.search_select} onChange={e=> setCarBrand(e)}>
                    <option value="">Modèles</option>
                    <option value="audi">1</option>
                    <option value="bmw">2</option>
                    <option value="citroen">3</option>
                    <option value="dacia">4</option>
                    <option value="fiat">5</option>
                    <option value="ford">5</option>
                </select>
             }
             <div class={style.search_title}>Sélectionnner la catégorie de votre pièce</div>
             <TreeView categories={props.categories} />
             <div class={style.search_text}> pièce selectionné : <span class={style.category_selected}>{props.categorySelected}</span></div>
             <button class={style.search_btn} onClick={() => handleSearch()}>rechercher</button>
        </div>
    );
}

export default Search;
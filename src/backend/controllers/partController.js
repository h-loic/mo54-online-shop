const Part = require('../models/part');

module.exports = {

    getAll : async (request, reply) => {
      try {
        let resultsPerPage = 6;
        let params = {};
        if(request.query.category) {
          params.category = request.query.category;
        }else{
          reply.code(500).send("aucune catégorie sélectionné");
        }
        if(request.query.lastId) {
            let lastPart = await Part.findById(request.query.lastId);
            params._id = {$lt: lastPart._id};
        } 
        let parts = await Part.find(params)
            .sort({"_id":-1})
            .limit(resultsPerPage)
        reply.code(200).send(parts);
    } catch (e) {
        reply.code(500).send(e);
    }
  }

};
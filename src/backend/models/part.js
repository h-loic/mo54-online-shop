const mongoose = require('mongoose');
const { Schema } = mongoose;

const partSchema = new Schema({
    name: String,
    price: Number,
    description: String,
    img: String,
    model: String,
    brand: String,
    category: String
});

const Part = mongoose.model('Part', partSchema);

module.exports = Part;
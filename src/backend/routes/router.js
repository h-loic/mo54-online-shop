const partController = require("../controllers/partController");

module.exports = (app) => {

    app.get('/api/parts', partController.getAll);
    
};
  